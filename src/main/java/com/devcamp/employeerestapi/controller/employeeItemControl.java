package com.devcamp.employeerestapi.controller;

import com.devcamp.employeerestapi.employee;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")

public class employeeItemControl {
    @GetMapping("/employees")
    public ArrayList<employee> getListEmployeeItem(){
    employee employee1 = new employee(1,"Nguyen", "Huong", 10000000);
    employee employee2 = new employee(2,"Tran", "B", 20000000);
    employee employee3 = new employee(3,"Nguyen", "C", 30000000);
    //in ra console
    System.out.println(employee1.toString());
    System.out.println(employee2.toString());
    System.out.println(employee3.toString());

    //tạo danh sách lớp InvoiceItem
    ArrayList<employee> employeeItems = new ArrayList<>();
    //thêm lần lượt các đối tuộng vào trong arraylist
    employeeItems.add(employee1);
    employeeItems.add(employee2);
    employeeItems.add(employee3);

    return employeeItems;
    }
}
